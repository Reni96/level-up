import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TsWikipageComponent } from './ts-wikipage.component';

describe('TsWikipageComponent', () => {
  let component: TsWikipageComponent;
  let fixture: ComponentFixture<TsWikipageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TsWikipageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsWikipageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
